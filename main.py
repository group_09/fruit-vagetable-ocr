#!/bin/python3

import flask
import hashlib
import os
import re
from multiprocessing import Queue, Process

import db
from config.config import listen_port, listen_host
from tflearn.model import recognize


basedir = os.path.abspath(os.path.dirname(__file__))
flask_app = flask.Flask(__name__, template_folder="templates", static_url_path='/static')
task_que = Queue()


@flask_app.route('/', methods=['GET'])
def index():
    """
    首页返回
    """
    host = flask.request.host_url
    prefix = flask.request.headers.get("X-Forwarded-Prefix")
    if prefix is not None:
        host = os.path.join(host, prefix)
    if host[-1] != "/":
        host += "/"
    return flask.render_template("index.html", **{"my_host": host})


@flask_app.route('/upload', methods=['POST'])
def upload_img():
    """
    上传图片
    """
    # print(flask.request.files)
    img = flask.request.files.get("upload_img")
    prefix = flask.request.headers.get("X-Forwarded-Prefix")
    host = flask.request.host_url
    root = "/"
    if prefix is not None:
        host = os.path.join(host, prefix)
        root = os.path.join(root, prefix)
    if host[-1] != "/":
        host += "/"
    if img is not None:
        # print(img.filename)
        suffix = os.path.splitext(img.filename)[-1]
        if suffix not in [".jpg", ".png"]:
            return flask.render_template("error.html", **{"error_text": "不支持的文件类型。"})
        content = img.read()
        md5hash = hashlib.md5(content)
        md5 = md5hash.hexdigest().lower()
        img_name = md5 + suffix
        filepath = f"{basedir}/static/uploads/{img_name}"
        with open(os.path.normpath(filepath), "wb") as f:
            f.write(content)
        db.delete_db(img_name)
        task_que.put(img_name)
        # print("Put img into queue")
        return flask.render_template("result.html", **{"img_name": img_name, "my_host": host, "index_root": root})
    return flask.render_template("index.html", **{"my_host": flask.request.url})


@flask_app.route('/check', methods=["GET"])
def result_check():
    """
    结果查询
    """
    img_name = flask.request.args.get("n")
    if img_name is not None and re.match(r"[0-9a-z]+\.[a-z]+", img_name):
        result = db.search_db(img_name)
    else:
        result = ''
    return flask.jsonify({'result': result})


def ai_run(q):
    """
    从消息队列取出并计算
    """
    while 1:
        img_name = q.get()
        print("New task:", img_name)
        result = recognize(img_name)
        print(result)
        db.update_db(img_name, result)


def main():
    pa = Process(target=ai_run, args=(task_que, ))
    pa.daemon = True
    pa.start()
    if pa.is_alive():
        print("Task process started.")
        flask_app.run(host=listen_host, port=listen_port, debug=False, threaded=True, processes=False)
        pa.join()


if __name__ == "__main__":
    main()
