import sqlite3
import os


basedir = os.path.abspath(os.path.dirname(__file__))
dbpath = os.path.join(basedir, "cache/result.db")


def init_db():
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS results '
                '(name VARCHAR(64), result VARCHAR(2048), '
                'PRIMARY KEY(name));')
    conn.commit()
    conn.close()


def update_db(name, result):
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur.execute('INSERT INTO results(name, result) VALUES(?, ?);', (name, result))
    conn.commit()
    conn.close()


def delete_db(name: str):
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur.execute('DELETE FROM results WHERE name=?;', (name, ))
    conn.commit()
    conn.close()


def search_db(name: str):
    conn = sqlite3.connect(dbpath)
    cur = conn.cursor()
    cur.execute("SELECT result FROM results WHERE name=?;", (name, ))
    ans = cur.fetchall()
    conn.close()
    if len(ans):
        # 查询成功 删除记录
        # delete_db(name)
        return ans[0][0]
    return ''


init_db()
