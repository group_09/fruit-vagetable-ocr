import os
import cv2
import numpy as np
from flask import Flask, render_template, request
from tensorflow.keras import models
import glob
import matplotlib.pyplot as plt
from tensorflow.keras import layers, Sequential, optimizers
import tensorflow as tf

app = Flask(__name__)

# 识别花，实例，openCV库
# 监督学习
# 数据集
path = "D:\\fruits\\"
# 图片设置
h = 100  # 长
w = 100  # 宽
c = 3  # 图片通道 = 3，RGB彩色

basedir = os.path.abspath(os.path.dirname(__file__))


# 读取图片
def read_img(path):
    # print(os.listdir(path))
    imgs = []
    labels = []  # 文件夹的索引作为 标签
    # 图片类别
    print( os.listdir(path))
    cate = [path + x for x in os.listdir(path) if os.path.isdir(path + x)]
    # 遍历文件夹，添加数据
    for index, folder in enumerate(cate): # 同时列出下标，和名字
        print(index,folder)
        # print(glob.glob(folder+'/*.jpg'))
        for im in glob.glob(folder + '/*.jpg'):
            # 读取图片
            img = cv2.imread(im)
            # 重置图片大小
            img = cv2.resize(img, (w, h))
            imgs.append(img)
            labels.append(index)
    return np.asarray(imgs, np.float32), np.asarray(labels, np.float32) # 将列表转为数组


if __name__ == '__main__':
    data, label = read_img(path)
    print(data,label)
    # 划分数据集
    from sklearn.model_selection import train_test_split

    X_train, X_test, y_train, y_test = train_test_split(data, label, test_size=0.21, random_state=7)

    # 归一化
    X_train = X_train/255.
    X_test = X_test/255.
    # 训练集的规模
    print("训练集：")
    print(X_train.shape)
    print("测试集：")
    print(X_test.shape)

    fruit_dict2 = {
        0: 'apple',
        1: 'banana',
        2: 'broccoli',
        3: 'grape',
        4: 'orange',
        5: 'pear',
        6: 'strawberry'
    }
    # 卷积神经网络
    model = Sequential([
        # layers.experimental.preprocessing.Rescaling(1. / 255.),
        layers.Conv2D(32, 5, padding="same", activation=tf.nn.relu),
        layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding="same"),

        # 随机丢弃
        layers.Dropout(0.2),

        layers.Conv2D(64, 3, padding="same", activation=tf.nn.relu),
        layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding="same"),

        layers.Dropout(0.15),

        layers.Conv2D(128, 3, padding="same", activation=tf.nn.relu),
        layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding="same"),

        # layers.Conv2D(128, 3, padding="same", activation=tf.nn.relu),
        # layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding="same"),

        # 将每一次特征，化为1维
        layers.Flatten(),
        # 全连接
        layers.Dense(256, activation=tf.nn.relu),
        layers.Dense(128, activation=tf.nn.relu),
        # 输出层
        layers.Dense(7, activation="softmax")

    ])
    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    model.fit(X_train, y_train, epochs=20, validation_data=(X_test, y_test), batch_size=64)

    # 保存加载整个模型结构
    model.save(os.path.join(basedir, "model.h5"))

    # 加载模型
    model = models.load_model(os.path.join(basedir, "model.h5"))

    test_path2 = "D:/TestImages/"
    test_path3 = "D:\\fruit_veg_testset\\testset\\"
    test_path = "D:\\fruit_test\\"
    test_imgs = []

    for im in glob.glob(test_path + "*.jpg"):
        img = cv2.imread(im)
        img = cv2.resize(img, (100, 100))
        test_imgs.append(img)

    test_imgs = np.asarray(test_imgs)

    # 模型预测,onehot编码
    pred = model.predict(test_imgs)
    print("测试数据有：")
    print(pred)
    for index in range(np.size(pred)):
        img = plt.imread(test_path + "test" + str(index + 1) + ".jpg")
        print("预测结果:", fruit_dict2[pred[index].tolist().index(1.0)])
        plt.imshow(img)
        plt.show()
