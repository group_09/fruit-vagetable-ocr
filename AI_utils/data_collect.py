
import os
import shutil


fruit_360_dir = r"C:\Users\weili\Downloads\Compressed\fruit360"
out_dir = r"C:\Users\weili\Downloads\Compressed\fruit360\dataset"

kinds = ["Apple", "Apricot", "Banana", "Blueberry", "Cantaloupe",
         "Carambula", "Cherry", "Clementine",
         "Fig", "Grape", "Kaki", "Kiwi",
         "Kumquats", "Lemon", "Lychee", "Mandarine", "Mango", "Maracuja", "Orange",
         "Peach", "Pear", "Physalis", "Pineapple", "Pitahaya", "Raspberry", "Strawberry",
         "Watermelon"]
trans = ["苹果", "杏", "香蕉", "蓝莓", "哈蜜瓜",
         "杨桃", "樱桃", "小柑橘",
         "无花果", "葡萄", "柿子", "弥猴桃",
         "金橘", "柠檬", "荔枝", "柑橘", "芒果", "百香果", "甜橙",
         "桃子", "梨子", "灯笼果", "菠萝", "火龙果", "树莓", "草莓",
         "西瓜"]

if len(kinds) != len(trans):
    print(len(kinds), len(trans))
    exit(1)

names = {}
for i in range(len(kinds)):
    names.update({kinds[i]: trans[i]})

fruit_360_dir = os.path.normpath(fruit_360_dir + "/fruits-360_dataset/fruits-360/Training")

# 目录遍历所有
dirs = os.listdir(fruit_360_dir)
for d in dirs:
    for n in kinds:
        if n in d:
            src = os.path.join(fruit_360_dir, d)
            des = os.path.join(out_dir, n)
            if not os.path.exists(des):
                os.mkdir(des, 0o755)
            fs = os.listdir(src)
            for f in fs:
                shutil.copyfile(os.path.join(src, f), os.path.join(des, f))

