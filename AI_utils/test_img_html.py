import os.path

import requests
import re

outdir = r"C:\Users\weili\Downloads\Compressed\fruit360\黄瓜"

# res = requests.get("https://www.yunguoxuan.com/class/")
# ans = re.findall(r"src=\"(.*\.jpg)\"", res.text)

url = "https://www.vcg.com/creative-image/huanggua/?page="
for i in range(1, 11):
    res = requests.get(url + f"{i}")
    ans = re.findall(r"\"src\":\"([^\"]*)\",", res.text)
    for a in ans:
        u = f"https:{a}"
        res = requests.get(u)
        with open(os.path.join(outdir, os.path.basename(u)), "wb") as f:
            f.write(res.content)
