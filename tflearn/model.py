import os
import cv2
import numpy as np

from tensorflow.keras import models

basedir = os.path.abspath(os.path.dirname(__file__))

fruit_dict2 = {
    0: '苹果',
    1: '香蕉',
    2: '西兰花',
    3: '葡萄',
    4: '橘子',
    5: '梨',
    6: '草莓'
}

model = models.load_model(os.path.normpath(f"{basedir}/../cache/model.h5"))


def recognize(im: str):
    """
    识别图片
    """
    img_path = os.path.normpath(f"{basedir}/../static/uploads/{im}")
    img = cv2.imread(img_path)
    # 重置图片大小
    img = cv2.resize(img, (100, 100))
    # 删除图片
    # os.remove(img_path)
    # print(img)
    test_img = np.asarray([img, ])
    pred = model.predict(test_img)
    # print(pred)
    ans = pred[0].tolist()
    print("max ", max(ans))
    return fruit_dict2[ans.index(max(ans))]
